﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EgitimMerkezi.Controllers
{
    public class HomeController : Controller
    {
        EgitimMerkeziEntities db = new EgitimMerkeziEntities();
        public ActionResult Index()
        {
            Models.HomeModel dto = new Models.HomeModel();
            dto.Duyurular = db.Duyurular.ToList();
            return View(dto);
        }

        [ChildActionOnly]
        public ActionResult _Slider()
        {
            var liste = db.Slider.Where(x => x.BaslangicTarih < DateTime.Now && x.BitisTarih > DateTime.Now).OrderByDescending(x => x.ID);
            return View(liste);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}